package com.stronglover.MailVerification.email;

public interface EmailSender {
    void Send(String to,String email);
}
